﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repozytorium.Models
{
    public class Ogloszenie
    {
        public Ogloszenie()
        {
            this.Ogloszenie_Kategoria = new HashSet<Ogloszenie_Kategoria>();
        }
        [Display(Name = "Id: ")]
        public int Id { get; set; }

        [Display(Name = "Treść ogłoszenia: ")]
        [MaxLength(600)]
        public string Tresc { get; set; }

        [Display(Name = "Tytuł ogłoszenia: ")]
        [MaxLength(70)]
        public string Tytul { get; set; }

        [Display(Name = "Data dodania: ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public System.DateTime DataDodania { get; set; }



        [Display(Name = "Stan: ")]
        [ScaffoldColumn(false)]
        public int Stan { get; set; }

        [Display(Name = "Data ostatniej edycji: ")]
        public DateTime? OstatnioEdytowane { get; set; }

        [Display(Name = "Id ostatniej edycji: ")]
        public string IdOstatnioEdytowane { get; set; }

        public string UzytkownikId { get; set; }
        public virtual ICollection<Ogloszenie_Kategoria>
            Ogloszenie_Kategoria
        { get; set; }
        public virtual Uzytkownik Uzytkownik { get; set; }
    }

}